package com.zhao;

public class Main {

    public static void main(String[] args) {
	    // Write a method with the name sumDigits that has one int parameter called number.
        //If parameter is >= 10 then the method should process the number and return sum of all digits,
        //otherwise return -1 to indicate an invalid value.
        //The numbers from 0-9 have 1 digit so we donft want to process them, also we donft want to process negative numbers,
        //so also return -1 for negative numbers.
        //For example calling the method sumDigits(125) should return 8 since 1 + 2 + 5 = 8.
        //Calling the method sumDigits(1) should return -1 as per requirements described above.
        //Add some code to the main method to test out the sumDigits method to determine
        //that it is working correctly for valid and invalid values passed as arguments.
        System.out.println("the sum of digits in number 125 is "+ sumDigits(125));
        System.out.println("the sum of digits in number 19 is "+ sumDigits(19));
        System.out.println("the sum of digits in number 1259 is "+ sumDigits(1259));
        System.out.println("the sum of digits in number -125 is "+ sumDigits(-125));
        System.out.println("the sum of digits in number 5 is "+ sumDigits(5));
    }
    private static int sumDigits (int number){
        if(number<10){
            return -1;
        }
        int sum =0;
        // smallestNumber(eg 125) = 123/10=12, 12*10=120, 125-120=5.
        while (number>0){
             int digit = number %10;
             sum += digit;
             // drop the smallest digit so 125 to 12 now.
            number /=10;
//            this is 125/10=12.5 then as an int it drops .5 so now is 12.
        }
        return sum;
    }
}
