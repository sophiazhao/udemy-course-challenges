package com.zhao;

public class HealthyBurger extends BasicBurger {
    private double egg;
    private double avocado;

    public HealthyBurger(String meat) {
        super("Healthy Burger", "brown rye", meat);
        this.egg = 1;
        this.avocado = 1.5;
    }

    public double addEgg(){
        this.price += egg;
        System.out.println("Additional egg is $" + this.egg + ", new total is $ " + this.price);
        return price;
    }
    public double addAvocado(){
        this.price += avocado;
        System.out.println("Additional avocado is $" +this.avocado + ", new total is $ " + this.price);
        return price;
    }

    @Override
    public double Hamburger() {
        double finalPrice = super.price;
        System.out.println(super.name +" on " + super.bunType + " bun with " + meat + " is $" + this.price);
        return finalPrice;
    }

}
