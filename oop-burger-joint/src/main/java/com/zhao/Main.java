package com.zhao;

public class Main {

    public static void main(String[] args) {

        BasicBurger basicBurger = new BasicBurger("Jack's", "white","beef");

        HealthyBurger healthyBurger = new HealthyBurger("chicken");
        System.out.println(basicBurger.addTomato());
        System.out.println(healthyBurger.addEgg());
        System.out.println(healthyBurger.addAvocado());
        System.out.println(healthyBurger.Hamburger());

        DeluxeHamburgerMeal deluxeHamburgerMeal = new DeluxeHamburgerMeal();
        System.out.println(deluxeHamburgerMeal.addChips());
        System.out.println(deluxeHamburgerMeal.addSoftDrink());
        System.out.println(deluxeHamburgerMeal.addLettuce());
        System.out.println(deluxeHamburgerMeal.selectMeal());
    }
}
