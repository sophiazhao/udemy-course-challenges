package com.zhao;

public class BasicBurger {
    String name;
    String bunType;
    String meat;
    double price;
    private double lettuce;
    private double tomato;
    private double cheese;
    private double onion;

    public BasicBurger(String name, String bunType, String meat) {
        this.name = name;
        this.bunType = bunType;
        this.meat = meat;
        this.price = 3;
        this.lettuce = 0.5;
        this.tomato = 0.75;
        this.onion = 0.4;
        this.cheese =0.8;
    }

    public double addLettuce(){
        this.price += lettuce;
        System.out.println("Additional lettuce is $" + this.lettuce + ", new total is $ " + this.price);
        return price;
    }
    public double addTomato(){
        this.price += tomato;
        System.out.println("Additional tomato is $" + this.tomato + ", new total is $ " + this.price);
        return price;
    }
    public double addCheese(){
        this.price += cheese;
        System.out.println("Additional cheese is $" +this.cheese + ", new total is $ " + this.price);
        return price;
    }
    public double addOnion(){
        this.price += onion;
        System.out.println("Additional onion is $" +this.onion + ", new total is $ " + this.price);
        return this.price;
    }

    public double Hamburger(){
        double finalPrice = this.price;
        System.out.println(this.name + " Hamburger on " + this.bunType + " bun with " + this.meat + " is $" + this.price);
        return finalPrice;
    }


}
