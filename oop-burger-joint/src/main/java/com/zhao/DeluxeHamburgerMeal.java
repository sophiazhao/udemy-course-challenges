package com.zhao;

public class DeluxeHamburgerMeal extends BasicBurger {
    double chips;
    double softDrink;


    public DeluxeHamburgerMeal() {
        super("Deluxe burger", "multi-grains","wagyu beef");
        this.price = 8;
        this.chips = 2;
        this.softDrink = 2.5;
    }

    public double addChips(){
        this.price += chips;
        System.out.println("add chips with deluxe burger is $" + chips);
        return price;
    }

    public double addSoftDrink(){
        this.price += softDrink;
        System.out.println("add a soft drink with deluxe burger is $" + softDrink);
        return price;
    }

    /*public DeluxeHamburgerMeal(String name, String bunType, String meat) {
        super(name, bunType, meat);
    }*/

    public double selectMeal(){
        System.out.println(name + " on "+ bunType + " bun with "+ meat + " comes with chips and a soft drink, total cost of this meal set is $" + this.price);
        return price;
    }

    @Override
    public double addLettuce() {
        System.out.println("Deluxe meal is set, cannot add additional toppings");
        return -1;
    }

    @Override
    public double addTomato() {
        System.out.println("Deluxe meal is set, cannot add additional toppings");
        return -1;
    }

    @Override
    public double addCheese() {
        System.out.println("Deluxe meal is set, cannot add additional toppings");
        return -1;
    }

    @Override
    public double addOnion() {
        System.out.println("Deluxe meal is set, cannot add additional toppings");
        return -1;
    }
}
